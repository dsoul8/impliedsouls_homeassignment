using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{

    public GameObject MainMenuItens;
    public PlayerMovement pMovement;
    public GameObject LooseItems;
    public TextMeshProUGUI   speedText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        speedText.text = "Forward Speed : " + pMovement.forwardSpeed;
    }

    public void Play()
    {
     //   SceneManager.LoadScene("Gameplay");
        MainMenuItens.SetActive(false);
        pMovement.GetComponent<Animator>().Play("Run");
        pMovement.forwardSpeed = 15;
    }

    public void LooseScreen()
    {
        Time.timeScale = 0;
        LooseItems.SetActive(true);
    }

    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Gameplay");
    }

    public void Level2()
    {
        Time.timeScale = 1;

        SceneManager.LoadScene("Gameplay-Level2");
    }

    public void Level3()
    {
        Time.timeScale = 1;

        SceneManager.LoadScene("Gameplay-Level3");
    }

    public void IncreasePlayerSpeed()
    {
        pMovement.forwardSpeed += 3;
    }
}
