using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{

    public Transform target;        //Public variable to store a reference to the player game object


    private Vector3 offset;            //Private variable to store the offset distance between the player and camera

    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = transform.position - target.transform.position;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {

        Vector3 newPosition = new Vector3(transform.position.x, transform.position.y, offset.z + target.position.z);
        transform.position = Vector3.Lerp(transform.position,newPosition,10*Time.deltaTime);
    }
}



//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;

//[RequireComponent(typeof(Rigidbody))]

//public class PlayerMovement : MonoBehaviour
//{
//    public float gravity = 20.0f;
//    public float jumpHeight = 2.5f;

//    Rigidbody r;
//    public bool grounded = false;
//    Vector3 defaultScale;
//    bool crouch = false;

//    // Start is called before the first frame update
//    void Start()
//    {
//        r = GetComponent<Rigidbody>();
//        r.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
//        r.freezeRotation = true;
//        r.useGravity = false;
//        defaultScale = transform.localScale;
//    }

//    void Update()
//    {
//        // Jump
//        if (Input.GetKeyDown(KeyCode.W) && grounded)
//        {
//            Debug.Log("-----Jumping------");
//            r.velocity = new Vector3(r.velocity.x, CalculateJumpVerticalSpeed(), r.velocity.z);
//        }

//        //Crouch
//        crouch = Input.GetKey(KeyCode.S);
//        if (crouch)
//        {
//            transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(defaultScale.x, defaultScale.y * 0.4f, defaultScale.z), Time.deltaTime * 7);
//        }
//        else
//        {
//            transform.localScale = Vector3.Lerp(transform.localScale, defaultScale, Time.deltaTime * 7);
//        }
//    }

//    // Update is called once per frame
//    void FixedUpdate()
//    {
//        // We apply gravity manually for more tuning control
//        r.AddForce(new Vector3(0, -gravity * r.mass, 0));

//        // grounded = false;
//    }

//    //void OnCollisionStay()
//    //{
//    //    grounded = true;
//    //}

//    float CalculateJumpVerticalSpeed()
//    {
//        // From the jump height and gravity we deduce the upwards speed 
//        // for the character to reach at the apex.
//        return Mathf.Sqrt(2 * jumpHeight * gravity);
//    }

//    //void OnCollisionEnter(Collision collision)
//    //{
//    //    if (collision.gameObject.tag == "Finish")
//    //    {
//    //        //print("GameOver!");
//    //        SC_GroundGenerator.instance.gameOver = true;
//    //    }
//    //}
//}