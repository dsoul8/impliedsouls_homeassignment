using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrickPlacements : MonoBehaviour
{

    public GameObject prefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if(Input.GetKeyDown(KeyCode.H))
        {
            Debug.Log("----Counter---------" + this.transform.childCount);
        }
    }
    
    public void AllignBricks()
    {

        int Count = this.transform.childCount;

        GameObject obj = Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity);

        obj.transform.SetParent(this.transform);

        obj.transform.localPosition = new Vector3(0, 1, -0.1f);

        obj.transform.localScale = new Vector3(-0.8f, -0.8f, -0.7f);

        int yPos = 0;
        for(int i =0;i<this.transform.childCount;i++)
        {
            if(i==0)
            {
                yPos = 1;
            }
            else
            {
                yPos += 1;
            }
            this.transform.GetChild(i).localPosition = new Vector3(0, yPos, -0.1f);
        }
    }


}
