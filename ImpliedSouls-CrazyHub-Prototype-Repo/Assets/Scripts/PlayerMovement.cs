using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 direction;
    public float forwardSpeed;
    private int desiredLane=1;
    public float laneDistance = 4;

    public GameObject Bricks_Holder;
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
     }

    void Update()
    {
        direction.z = forwardSpeed;

        if(SwipeManager.swiperight)
        {
            desiredLane++;
            if(desiredLane==3)
            {
                desiredLane = 2;
            }
        }
        if (SwipeManager.swipeleft)
        {
            desiredLane--;
            if (desiredLane == -1)
            {
                desiredLane = 0;
            }
        }

        Vector3 targetPosition = transform.position.z * transform.forward + transform.position.y * transform.up;

        if (desiredLane == 0)
        {
            targetPosition += Vector3.left * laneDistance;
        }
        else if (desiredLane == 2)
        {
            targetPosition += Vector3.right * laneDistance;
        }

        transform.position = Vector3.Lerp(transform.position,targetPosition,10*Time.deltaTime);
        controller.center = controller.center;


    }
    private void FixedUpdate()
    {
        Physics.SyncTransforms();
        controller.Move(direction * Time.fixedDeltaTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Brick")
        {
            other.gameObject.SetActive(false);
            Bricks_Holder.GetComponent<BrickPlacements>().AllignBricks();
        }

        if (other.tag == "Brick_Road")
        {
            if (Bricks_Holder.transform.childCount>0)
            {
                other.GetComponent<MeshRenderer>().enabled = true;
                for (int i = 0; i < Bricks_Holder.transform.childCount; i++)
                {
                    int bottommost = Bricks_Holder.transform.childCount - 1;
                    Destroy(Bricks_Holder.transform.GetChild(bottommost).gameObject);
                }
            }
            else
            {
                GameOver();

            }

        }

        if (other.tag == "Dead")
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        forwardSpeed = 0;
        this.GetComponent<Animator>().Play("Idle");
        GameObject.Find("Canvas").GetComponent<UIManager>().LooseScreen();
        Debug.Log("-----Dead-----");
    }

}