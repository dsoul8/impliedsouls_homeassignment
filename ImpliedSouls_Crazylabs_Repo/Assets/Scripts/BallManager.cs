using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening.Core;
using DG.Tweening;

public class BallManager : MonoBehaviour
{

    public Transform Ball;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveToDestination(float des)
    {
        Ball.DOMoveZ(des, 5);
    }
}
