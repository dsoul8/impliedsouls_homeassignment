using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SneakerManager : MonoBehaviour
{
    public GameObject Player;
    public Transform Rightfoot;
    public Transform LeftFoot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IncreaseSneakerSize()
    {

        if(Rightfoot.localScale.x>=3)
        {
            return;
        }

        Rightfoot.localScale  =  new Vector3(Rightfoot.localScale.x+0.2f, Rightfoot.localScale.y + 0.2f, Rightfoot.localScale.z + 0.2f);
        LeftFoot.localScale = new Vector3(LeftFoot.localScale.x + 0.2f, LeftFoot.localScale.y + 0.2f, LeftFoot.localScale.z + 0.2f);

    }

    public void DecreaseSneakerSize()
    {

        if (Rightfoot.localScale.x <= 1)
        {
            return;
        }

        Rightfoot.localScale = new Vector3(Rightfoot.localScale.x - 0.5f, Rightfoot.localScale.y - 0.5f, Rightfoot.localScale.z - 0.5f);
        LeftFoot.localScale = new Vector3(LeftFoot.localScale.x - 0.5f, LeftFoot.localScale.y - 0.5f, LeftFoot.localScale.z - 0.5f);
    }



}
